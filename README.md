# PBMC Data Classification 

Individual faculty project done within the "Data mining 2" course.

Labeling of cell types by gene expression patterns from biological samples. Model training using classification algorithms such as Neural networks, SVC, Decision three etc. The goal of the project is to compare the power of various algorithms and parametars within one. 

Repository contains seminary work in Serbian and code executed using Jupyter Notebook.